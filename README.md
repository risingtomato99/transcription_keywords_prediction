# transcription_keywords_prediction

This repository contain group project of Data Science class on S2 CompSci UGM.


## Feautures 
- Predict keywords from medical transcription
- Download the predicted keywords as csv

## Installation 

1. clone this repository
```bash
git clone https://gitlab.com/risingtomato99/transcription_keywords_prediction
```

2. change the directory to the repository
```bash
cd transcription_keywords_prediction
```

3. install all python module requirements (using python3/pip3)
```
pip install -r requirements.txt
```

4. run the webserver with this command :
```
chmod +x run.sh ; ./run.sh
```

5. you should see working webserver on http://localhost:5000/
6. Example of website preview
![](prev_web.JPG)
