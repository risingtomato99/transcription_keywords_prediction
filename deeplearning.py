import pickle
import random
from nltk.corpus import stopwords
stop_words = set(stopwords.words("english"))
import numpy as np
import os
import argparse
import json

##--------------------------------------- TENSORFLOW ----------------------------------------------------##
import tensorflow as tf
from tensorflow.python.keras.models import model_from_json
from tensorflow.python.keras.preprocessing.text import Tokenizer
from tensorflow.python.keras.preprocessing.sequence import pad_sequences
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import base64
import io 


json_file = open("model.json", "r")
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)
model.load_weights("model.h5")
tokenizer = pickle.load(open("tokenizer.pickle","rb"))

def predict(input_):
    new_t = Tokenizer()
    new_t.fit_on_texts([input_])
    tokens = [i for i in new_t.word_index.keys()]
    actual_tokens = new_t.texts_to_sequences([input_])
    inv_map_tokens = {v: k for k, v in new_t.word_index.items()}
    actual_tokens = [inv_map_tokens[i] for i in actual_tokens[0]]
    tokens = actual_tokens
    input_ = tokenizer.texts_to_sequences([input_])
    input_ = pad_sequences(input_, padding = "post", truncating = "post", maxlen = 25, value = 0)
    output = model.predict([input_])
    output = np.argmin(output, axis = -1)
    where_ = np.where(output[0] == 1)[0]
    output_keywords = np.take(tokens, where_)
    output_keywords = [i for i in output_keywords if i not in stop_words]
    output_keywords = list(set(output_keywords))

    return json.dumps(output_keywords)

def make_word_cloud(input_):
    wc = WordCloud(
        background_color='white',
        max_words=2000,
        stopwords=stop_words
    )
    wc.generate(input_)
    plt.imshow(wc, interpolation='bilinear')
    plt.axis('off')
    pic_IObytes = io.BytesIO()
    plt.savefig(pic_IObytes,  format='png')
    pic_IObytes.seek(0)
    pic_b64 = base64.b64encode(pic_IObytes.read())
    return pic_b64