from flask import Flask, render_template, request
from deeplearning import *

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/predict", methods=["POST"])
def predict_keyword():        
    body = request.form.get("body")
    predict_result = predict(body)
    return predict_result

@app.route("/wordcloud", methods=["POST"])
def generate_wc():        
    body = request.form.get("body")
    img_b64 = make_word_cloud(body)
    return img_b64