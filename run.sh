#!/bin/sh

export FLASK_APP=webserver.py
export FLASK_DEBUG=1

python3 -c "import nltk; nltk.download('stopwords')"

flask run --host 0.0.0.0
